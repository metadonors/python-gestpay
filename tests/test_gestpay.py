import os
import logging

logging.basicConfig(level=logging.WARNING)

logger = logging.getLogger()

from pygestpay import GestPAY

GESTPAY_ID = os.getenv('GESTPAY_ID', False)

class TestGestpay:
    def test_get_payment_page_url(self):    
        gestpay = GestPAY(shop_login=GESTPAY_ID, test=True)

        data = {
            "amount": 10.0,
            "transaction_id": "1234567890"
        }
        data = {'amount': '11.0', 'buyer_name': 'Administrator', 'buyer_email': 'admin@example.com', 'transaction_id': 'SO004-16'} 

        url = gestpay.get_payment_page_url(**data)
        
        assert url.startswith("https://sandbox.gestpay.net/pagam/pagam.aspx?a={shop_login}&".format(shop_login=GESTPAY_ID))

    def test_card_transaction_success(self):    
        gestpay = GestPAY(shop_login=GESTPAY_ID, test=True)

        data = {
            'amount': 10.0,
            'card_number': '4775718800002026',
            'exp_month': '05',
            'exp_year': '27',
            'transaction_id': '1234567890'
        }

        res = gestpay.card_transaction(**data)
        assert res['TransactionResult'] == "OK"

    def test_card_transaction_failure(self):    
        gestpay = GestPAY(shop_login=GESTPAY_ID, test=True)

        data = {
            'amount': 10.0,
            'card_number': '4775718800003024',
            'exp_month': '05',
            'exp_year': '27',
            'transaction_id': '1234567890'
        }

        res = gestpay.card_transaction(**data)
        assert res['TransactionResult'] == "KO"


    def test_card_check_card(self):    
        gestpay = GestPAY(shop_login=GESTPAY_ID, test=True)

        data = {
            'card_number': '5413330002001015',
            'exp_month': '05',
            'exp_year': '27',
        }

        res = gestpay.check_card(**data)

        assert res['TransactionResult'] == "OK"
        assert res['CompanyDescription'] == "MASTERCARD"
        assert res['CheckDigit'] == "OK"

        data = {
            'card_number': 'abcd1243556',
            'exp_month': '05',
            'exp_year': '27',
        }

        res = gestpay.check_card(**data)
        assert res['TransactionResult'] == "KO"
        assert res['TransactionErrorCode'] == "1118"